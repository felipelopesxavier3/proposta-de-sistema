<?php

namespace app\controllers;

use Yii;
use app\models\FMinistra;
use app\models\FMinistraSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FMinistraController implements the CRUD actions for FMinistra model.
 */
class FMinistraController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FMinistra models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FMinistraSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FMinistra model.
     * @param integer $materia
     * @param integer $professor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($materia, $professor)
    {
        return $this->render('view', [
            'model' => $this->findModel($materia, $professor),
        ]);
    }

    /**
     * Creates a new FMinistra model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FMinistra();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'materia' => $model->materia, 'professor' => $model->professor]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FMinistra model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $materia
     * @param integer $professor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($materia, $professor)
    {
        $model = $this->findModel($materia, $professor);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'materia' => $model->materia, 'professor' => $model->professor]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FMinistra model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $materia
     * @param integer $professor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($materia, $professor)
    {
        $this->findModel($materia, $professor)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FMinistra model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $materia
     * @param integer $professor
     * @return FMinistra the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($materia, $professor)
    {
        if (($model = FMinistra::findOne(['materia' => $materia, 'professor' => $professor])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
