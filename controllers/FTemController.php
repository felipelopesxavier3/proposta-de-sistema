<?php

namespace app\controllers;

use Yii;
use app\models\FTem;
use app\models\FTemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FTemController implements the CRUD actions for FTem model.
 */
class FTemController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FTem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FTemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FTem model.
     * @param integer $aulas
     * @param integer $materia
     * @param integer $professor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($aulas, $materia, $professor)
    {
        return $this->render('view', [
            'model' => $this->findModel($aulas, $materia, $professor),
        ]);
    }

    /**
     * Creates a new FTem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FTem();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'aulas' => $model->aulas, 'materia' => $model->materia, 'professor' => $model->professor]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FTem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $aulas
     * @param integer $materia
     * @param integer $professor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($aulas, $materia, $professor)
    {
        $model = $this->findModel($aulas, $materia, $professor);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'aulas' => $model->aulas, 'materia' => $model->materia, 'professor' => $model->professor]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FTem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $aulas
     * @param integer $materia
     * @param integer $professor
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($aulas, $materia, $professor)
    {
        $this->findModel($aulas, $materia, $professor)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FTem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $aulas
     * @param integer $materia
     * @param integer $professor
     * @return FTem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($aulas, $materia, $professor)
    {
        if (($model = FTem::findOne(['aulas' => $aulas, 'materia' => $materia, 'professor' => $professor])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
