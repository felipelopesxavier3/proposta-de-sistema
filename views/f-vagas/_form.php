<?php
use yii\helpers\ArrayHelper;
use app\models\FAulas;

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FVagas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fvagas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'data')->textInput(['type'=>'date']) ?>

    <?= $form->field($model, 'aulas')->
       dropDownList(ArrayHelper::map(FAulas::find()
           ->orderBy('Hinicio')
           ->all(),'ID','Hinicio'),
           ['prompt' => 'Selecione uma aula'] )
?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
