<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FVagas */

$this->title = 'Create F Vagas';
$this->params['breadcrumbs'][] = ['label' => 'F Vagas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fvagas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
