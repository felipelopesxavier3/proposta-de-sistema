<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FExtra */

$this->title = 'Update F Extra: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'F Extras', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fextra-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
