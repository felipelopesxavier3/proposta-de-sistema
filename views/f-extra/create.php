<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FExtra */

$this->title = 'Create F Extra';
$this->params['breadcrumbs'][] = ['label' => 'F Extras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fextra-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
