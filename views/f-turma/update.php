<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FTurma */

$this->title = 'Update F Turma: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'F Turmas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fturma-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
