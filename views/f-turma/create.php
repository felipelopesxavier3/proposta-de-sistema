<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FTurma */

$this->title = 'Create F Turma';
$this->params['breadcrumbs'][] = ['label' => 'F Turmas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fturma-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
