<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FProfessor */

$this->title = 'Create F Professor';
$this->params['breadcrumbs'][] = ['label' => 'F Professors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fprofessor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
