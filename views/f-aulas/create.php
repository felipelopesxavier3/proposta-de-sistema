<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FAulas */

$this->title = 'Create F Aulas';
$this->params['breadcrumbs'][] = ['label' => 'F Aulas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faulas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
