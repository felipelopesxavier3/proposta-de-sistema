<?php

use yii\helpers\ArrayHelper;
use app\models\FTurma;

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\FAulas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faulas-form">

    
    
<?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model,'Diadasemana')->dropDownList([
'1'=>'Domingo',
'2'=>'Segunda',
'3'=>'Terça',
'4'=>'Quarta',
'5'=>'Quinta',
'6'=>'Sexta',
'7'=>'Sábado'
],
['prompt' => 'Selecione um dia'] );
 ?>

    <?= $form->field($model, 'Hinicio')->textInput() ?>

    <?= $form->field($model, 'Hfim')->textInput() ?>

    <?= $form->field($model, 'turma_ID')->
       dropDownList(ArrayHelper::map(FTurma::find()
           ->orderBy('nome')
           ->all(),'ID','nome'),
           ['prompt' => 'Selecione uma turma'] )
?>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
   

</div>
