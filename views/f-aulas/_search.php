<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FAulasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faulas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Diadasemana') ?>

    <?= $form->field($model, 'Hinicio') ?>

    <?= $form->field($model, 'Hfim') ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'turma_ID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
