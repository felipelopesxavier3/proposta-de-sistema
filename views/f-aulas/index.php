<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FAulasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'F Aulas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faulas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Adicionar aula', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Diadasemana',
            'Hinicio',
            'Hfim',
            'ID',
            ['attribute'=>'turma.nome','label'=>'Turma'],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
