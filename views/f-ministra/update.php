<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FMinistra */

$this->title = 'Update F Ministra: ' . $model->materia;
$this->params['breadcrumbs'][] = ['label' => 'F Ministras', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->materia, 'url' => ['view', 'materia' => $model->materia, 'professor' => $model->professor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fministra-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
