<?php
use yii\helpers\ArrayHelper;
use app\models\FMateria;


use app\models\FProfessor;

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FMinistra */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fministra-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'materia')->
       dropDownList(ArrayHelper::map(FMateria::find()
           ->orderBy('nome')
           ->all(),'ID','nome'),
           ['prompt' => 'Selecione uma matéria'] )
?>

<?= $form->field($model, 'professor')->
       dropDownList(ArrayHelper::map(FProfessor::find()
           ->orderBy('nome')
           ->all(),'ID','nome'),
           ['prompt' => 'Selecione um professor'] )
?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
