<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FMinistra */

$this->title = $model->materia;
$this->params['breadcrumbs'][] = ['label' => 'F Ministras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="fministra-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'materia' => $model->materia, 'professor' => $model->professor], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Deletar', ['delete', 'materia' => $model->materia, 'professor' => $model->professor], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Tem certeza que deseja deletar este item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'materia',
            'professor',
        ],
    ]) ?>

</div>
