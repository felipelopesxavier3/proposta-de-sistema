<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FMinistraSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'F Ministras';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fministra-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create F Ministra', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute'=>'materia0.nome', 'label'=>'Matéria'],
            ['attribute'=>'professor0.nome', 'label'=>'Professor'],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
