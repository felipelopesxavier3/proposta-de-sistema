<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FTem */

$this->title = 'Update F Tem: ' . $model->aulas;
$this->params['breadcrumbs'][] = ['label' => 'F Tems', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->aulas, 'url' => ['view', 'aulas' => $model->aulas, 'materia' => $model->materia, 'professor' => $model->professor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ftem-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
