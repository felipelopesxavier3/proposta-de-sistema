<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FTem */

$this->title = $model->aulas;
$this->params['breadcrumbs'][] = ['label' => 'F Tems', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ftem-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'aulas' => $model->aulas, 'materia' => $model->materia, 'professor' => $model->professor], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Deletar', ['delete', 'aulas' => $model->aulas, 'materia' => $model->materia, 'professor' => $model->professor], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Tem certeza que deseja deletar este item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'aulas',
            'materia',
            'professor',
        ],
    ]) ?>

</div>
