<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FTem */

$this->title = 'Create F Tem';
$this->params['breadcrumbs'][] = ['label' => 'F Tems', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ftem-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
