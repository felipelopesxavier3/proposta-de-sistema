<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FTemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'F Tems';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ftem-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Inserir', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute'=>'aulasid.Hinicio', 'label'=>'Aulas'],
            ['attribute'=>'materiaid.nome', 'label'=>'Matéria'],
            ['attribute'=>'professorid.nome', 'label'=>'Professor'],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
