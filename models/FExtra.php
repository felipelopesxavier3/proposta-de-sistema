<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "f_extra".
 *
 * @property string $descricao
 * @property string $data
 * @property string $Hinicio
 * @property string $Hfim
 * @property int $ID
 * @property int $turma_ID
 *
 * @property FTurma $turma
 */
class FExtra extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'f_extra';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data', 'Hinicio', 'Hfim'], 'safe'],
            [['turma_ID'], 'integer'],
            [['descricao'], 'string', 'max' => 50],
            [['turma_ID'], 'exist', 'skipOnError' => true, 'targetClass' => FTurma::className(), 'targetAttribute' => ['turma_ID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'descricao' => 'Descrição',
            'data' => 'Data',
            'Hinicio' => 'Horário de inicio',
            'Hfim' => 'Horário de fim',
            'ID' => 'ID',
            'turma_ID' => 'Turma',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTurma()
    {
        return $this->hasOne(FTurma::className(), ['ID' => 'turma_ID']);
    }
}
