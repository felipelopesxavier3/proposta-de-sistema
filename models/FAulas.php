<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "f_aulas".
 *
 * @property string $Diadasemana
 * @property string $Hinicio
 * @property string $Hfim
 * @property int $ID
 * @property int $turma_ID
 *
 * @property FTurma $turma
 * @property FTem[] $fTems
 * @property FMinistra[] $materias
 * @property FVagas[] $fVagas
 */
class FAulas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'f_aulas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Hinicio', 'Hfim'], 'safe'],
            [['turma_ID'], 'integer'],
            [['Diadasemana'], 'string', 'max' => 50],
            [['turma_ID'], 'exist', 'skipOnError' => true, 'targetClass' => FTurma::className(), 'targetAttribute' => ['turma_ID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Diadasemana' => 'Dia da semana',
            'Hinicio' => 'Horário de início',
            'Hfim' => 'Horário de fim',
            'ID' => 'ID',
            'turma_ID' => 'Turma',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTurma()
    {
        return $this->hasOne(FTurma::className(), ['ID' => 'turma_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFTems()
    {
        return $this->hasMany(FTem::className(), ['aulas' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterias()
    {
        return $this->hasMany(FMinistra::className(), ['materia' => 'materia', 'professor' => 'professor'])->viaTable('f_tem', ['aulas' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFVagas()
    {
        return $this->hasMany(FVagas::className(), ['aulas_ID' => 'ID']);
    }
}
