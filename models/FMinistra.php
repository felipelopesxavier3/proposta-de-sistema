<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "f_ministra".
 *
 * @property int $materia
 * @property int $professor
 *
 * @property FMateria $materia0
 * @property FProfessor $professor0
 * @property FTem[] $fTems
 * @property FAulas[] $aulas
 */
class FMinistra extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'f_ministra';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['materia', 'professor'], 'required'],
            [['materia', 'professor'], 'integer'],
            [['materia', 'professor'], 'unique', 'targetAttribute' => ['materia', 'professor']],
            [['materia'], 'exist', 'skipOnError' => true, 'targetClass' => FMateria::className(), 'targetAttribute' => ['materia' => 'ID']],
            [['professor'], 'exist', 'skipOnError' => true, 'targetClass' => FProfessor::className(), 'targetAttribute' => ['professor' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'materia' => 'Materia',
            'professor' => 'Professor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMateria0()
    {
        return $this->hasOne(FMateria::className(), ['ID' => 'materia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfessor0()
    {
        return $this->hasOne(FProfessor::className(), ['ID' => 'professor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFTems()
    {
        return $this->hasMany(FTem::className(), ['materia' => 'materia', 'professor' => 'professor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAulas()
    {
        return $this->hasMany(FAulas::className(), ['ID' => 'aulas'])->viaTable('f_tem', ['materia' => 'materia', 'professor' => 'professor']);
    }
}
