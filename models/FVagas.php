<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "f_vagas".
 *
 * @property int $ID
 * @property string $data
 * @property int $aulas_ID
 *
 * @property FAulas $aulas
 */
class FVagas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'f_vagas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data'], 'safe'],
            [['aulas_ID'], 'integer'],
            [['aulas_ID'], 'exist', 'skipOnError' => true, 'targetClass' => FAulas::className(), 'targetAttribute' => ['aulas_ID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'data' => 'Data',
            'aulas_ID' => 'Aulas',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAulas()
    {
        return $this->hasOne(FAulas::className(), ['ID' => 'aulas_ID']);
    }
}
