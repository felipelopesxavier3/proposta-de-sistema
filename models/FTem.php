<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "f_tem".
 *
 * @property int $aulas
 * @property int $materia
 * @property int $professor
 *
 * @property FAulas $aulasid
 * @property FMinistra $materiaid
 * * @property FProfessor $professorid
 */
class FTem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'f_tem';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['aulas', 'materia', 'professor'], 'required'],
            [['aulas', 'materia', 'professor'], 'integer'],
            [['aulas', 'materia', 'professor'], 'unique', 'targetAttribute' => ['aulas', 'materia', 'professor']],
            [['aulas'], 'exist', 'skipOnError' => true, 'targetClass' => FAulas::className(), 'targetAttribute' => ['aulas' => 'ID']],
            [['materia', 'professor'], 'exist', 'skipOnError' => true, 'targetClass' => FMinistra::className(), 'targetAttribute' => ['materia' => 'materia', 'professor' => 'professor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'aulas' => 'Aulas',
            'materia' => 'Matéria',
            'professor' => 'Professor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAulasid()
    {
        return $this->hasOne(FAulas::className(), ['ID' => 'aulas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMateriaid()
    {
        return $this->hasOne(FMateria::className(), ['ID' => 'materia']);
    }

    public function getProfessorid()
    {
        return $this->hasOne(FProfessor::className(), ['ID' => 'professor']);
    }
}
