<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "f_materia".
 *
 * @property int $ID
 * @property string $nome
 *
 * @property FMinistra[] $fMinistras
 */
class FMateria extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'f_materia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'nome' => 'Nome',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFMinistras()
    {
        return $this->hasMany(FMinistra::className(), ['materia' => 'ID']);
    }
}
