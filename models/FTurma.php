<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "f_turma".
 *
 * @property int $ID
 * @property string $nome
 *
 * @property FAulas[] $fAulas
 * @property FExtra[] $fExtras
 */
class FTurma extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'f_turma';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'nome' => 'Nome',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFAulas()
    {
        return $this->hasMany(FAulas::className(), ['turma_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFExtras()
    {
        return $this->hasMany(FExtra::className(), ['turma_ID' => 'ID']);
    }
}
